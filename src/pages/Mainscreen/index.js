import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Linking,
  Alert,
} from 'react-native';
import Share from 'react-native-share';
import auth from '@react-native-firebase/auth';
import {GoogleSignin} from '@react-native-community/google-signin';

const MainScreen = () => {
  const message = 'TOLONG AKU URGENT!!!';
  const mobileNumber = '6281218169470';
  const [authenticated, setAutheticated] = useState(false);

  GoogleSignin.configure({
    webClientId:
      '1048384984874-073lqsm0t3tu9elbtgpbn396907nf093.apps.googleusercontent.com',
  });

  async function onGoogleButtonPress() {
    // Get the users ID token
    const {idToken} = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);
    if (googleCredential) {
      setAutheticated(true);
    }
    // Sign-in the user with the credential
    return auth().signInWithCredential(googleCredential);
  }

  const shareOptions = {
    title: 'Share Via',
    message,
    social: Share.Social.WHATSAPP,
    whatsAppNumber: mobileNumber,
  };
  const sosAction = () => {
    // Share.shareSingle(shareOptions)
    //   .then(res => {
    //     console.log('berhasil');
    //   })
    //   .catch(err => console.log(err));
    onGoogleButtonPress();
  };

  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity style={styles.sosButton} onPress={sosAction}>
          <Text style={styles.sosText}>SOS</Text>
        </TouchableOpacity>
      </View>
      {authenticated && (
        <View style={styles.container}>
          <TouchableOpacity style={styles.sosButton} onPress={sosAction}>
            <Text style={styles.sosText}>SOS</Text>
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};
export default MainScreen;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: '100%',
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  sosButton: {
    width: 200,
    height: 200,
    borderRadius: 100,
    backgroundColor: 'yellow',
    justifyContent: 'center',
  },
  sosText: {
    fontSize: 50,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
