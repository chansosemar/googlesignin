// __tests__/Login-page-test.js
import 'react-native';
import React from 'react';
import MainScreen from '../src/pages/Mainscreen';
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const tree = renderer.create(<MainScreen />).toJSON();
  expect(tree).toMatchSnapshot();
});
